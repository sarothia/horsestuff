


with firstBrunstIds (brunstId) as
(SELECT
 min(Brunst_ID) firstBrunst ---Find the first brunst for each season
  FROM [dbo].[Brunst] b
group by b.Säsongs_ID
)

select distinct brunst.Registreringsnummer from [dbo].[Behandlingstillfallen] b -- find unique horse IDs
inner join [dbo].[Brunst] brunst on (b.[Brunst ID] = brunst.Brunst_ID) where  -- join brunst and behandling, in order to be able to filter on both draktig and sperm given
b.[Brunst ID] in (select * from firstBrunstIds) ---find the behandlings in the "first brunsts"
and (b.[Natural cover] ='TRUE' or b.[AI frusen] ='TRUE' or b.[AI färsk] ='TRUE' or b.[AI kyld] ='TRUE' or b.[AI okänd spermatyp]='TRUE' )-- find all behandlinger which was given seamen
and brunst.Dräktig_dag_15 ='TRUE' -- find the brunsts where the horse got preggo