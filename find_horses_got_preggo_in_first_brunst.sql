with firstBrunstIds (brunstId) as
(SELECT
 min(Brunst_ID) firstBrunst ---Find the first brunst for each season
  FROM [dbo].[Brunst] b
group by b.Säsongs_ID
)

select distinct Registreringsnummer from [dbo].[Brunst] where Brunst_ID in (SELECT * from firstBrunstIds)
 and Dräktig_dag_15 = 'TRUE'

