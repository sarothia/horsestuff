/****** Script for SelectTopNRows command from SSMS  ******/

with bob (Registreringsnummer, Namn) as ( select Registreringnummer, Namn from [dbo].[generalInfo])

SELECT TOP (1000) bob.[Registreringsnummer], bob.Namn
      ,[Säsongs_ID]
      ,[Brunst_ID]
      ,[Dräktig_dag_15]
      ,[Tvilling]
      ,[Klämning]
      ,[Inducerad_abort_tvilling]
      ,[Levande_föl]
  FROM [MalinDb].[dbo].[Brunst]
  inner join bob on Brunst.[Registreringsnummer] = bob.Registreringsnummer